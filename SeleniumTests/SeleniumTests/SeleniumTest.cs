﻿namespace SeleniumTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.IE;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.PhantomJS;
    using System;
    using System.Drawing.Imaging;
    using System.Drawing;

    [TestClass]
    public class Test1
    {
        private string baseURL = "http://www.google.com";
        private IWebDriver driver;
        public TestContext TestContext { get; set; }

        [TestMethod]
        [TestCategory("Selenium")]
        [Priority(1)]
        [Owner("Anton")]

        public void Method1()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-popup-blocking");
            options.AddArgument("--disable-notifications");
            options.AddArgument("--no-sandbox");
            driver = new ChromeDriver(options);

            driver.Navigate().GoToUrl(baseURL);
            IWebElement findSearchBar = driver.FindElement(By.Name("q"));
            findSearchBar.SendKeys("Titan Gate Varna");
            findSearchBar.Submit();

        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            //driver.Quit();
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }
    }
}